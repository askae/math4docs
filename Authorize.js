
function getMath4OfficeService() {
  //DocumentApp.getUi().alert("Hello")
  const scope = "openid profile license_api"
  // Create a new service with the given name. The name will be used when
  // persisting the authorized token, so ensure it is unique within the
  // scope of the property store.
  return OAuth2.createService('math4office')

    // Set the endpoint URLs, which are the same for all Google services.
    .setAuthorizationBaseUrl('https://math4office.com/connect/authorize')
    .setTokenUrl('https://math4office.com/connect/token')

    // Set the client ID and secret, from the Google Developers Console.
    .setClientId('math4docs') //TODO: create these and fill
    .setClientSecret('ThisIsSomeRandomSecret')

    // Set the name of the callback function in the script referenced
    // above that should be invoked to complete the OAuth flow.
    .setCallbackFunction('authCallback')

    // Set the property store where authorized tokens should be persisted.
    .setPropertyStore(PropertiesService.getUserProperties())

    // Set cache to be on safe side
    .setCache(CacheService.getUserCache())

    // Use lock to prevent race around
      .setLock(LockService.getUserLock())

    // Set the scopes to request (space-separated for Google services).
    .setScope("openid profile license_api offline_access")
}

function authCallback(request) {
  const template = HtmlService.createTemplateFromFile('Callback')
  template.isSignedIn = false
  template.error = null
  var title
  try {
    const m4oService = getMath4OfficeService();
    const isAuthorized = m4oService.handleCallback(request);
    template.isSignedIn = isAuthorized
    title = isAuthorized ? 'Login sucessful.' : 'Access denied.'

  } catch(e) {
    template.error = e
    title = 'Access error.'
  }
  template.title = title
  return template.evaluate()
          .setTitle(title)
}

function getAuthUrl() {
  const service = getMath4OfficeService()
  const authUrl = service.getAuthorizationUrl()
  return authUrl
}

function logOut() {
  const service = getMath4OfficeService()
  service.reset()
}

function isLoggedIn() {
  const service = getMath4OfficeService();
  return service.hasAccess();
}

function getUserData() {
  const userinfoEndpoint = "https://math4office.com/connect/userinfo";
  var service = getMath4OfficeService();
  if (!service.hasAccess()) {
    return null;
  }

  const accessToken = service.getAccessToken();
  const response = UrlFetchApp.fetch(userinfoEndpoint, {
    headers: {
      Authorization: 'Bearer ' + accessToken
    }
  });
  var result = JSON.parse(response.getContentText());
  Logger.log(result);
  if (response.getResponseCode() === 200) {
    return result;
  } else {
    return null;
  }
}
