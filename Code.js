/**
// test Ankur
 * @OnlyCurrentDoc
 * The above comment directs Apps Script to limit the scope of file
 * access for this add-on. It specifies that this add-on will only
 * attempt to read or modify the files in which the add-on is used,
 * and not all of the user's files. The authorization request message
 * presented to users will reflect this limited scope.
 */
 
/**
 * Creates a menu entry in the Google Docs UI when the document is opened.
 * This method is only used by the regular add-on, and is never called by
 * the mobile add-on version.
 *
 * @param {object} e The event parameter for a simple onOpen trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode.
 */
function onOpen(e) {
  DocumentApp.getUi().createAddonMenu()
      .addItem('Start', 'showSidebar')
       .addItem('Settings', 'openSettings')
       .addSeparator()
       .addItem('Account', 'openAccount')
       //.addSeparator()
       //.addItem('Help', 'openHelp')
       .addToUi(); 
}

/**
 * Runs when the add-on is installed.
 * This method is only used by the regular add-on, and is never called by
 * the mobile add-on version.
 *
 * @param {object} e The event parameter for a simple onInstall trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode. (In practice, onInstall triggers always
 *     run in AuthMode.FULL, but onOpen triggers may be AuthMode.LIMITED or
 *     AuthMode.NONE.)
 */
function openSettings()
{
  if(isLoggedIn()) {
   var html = HtmlService.createHtmlOutputFromFile('Settings')
   .setHeight(510)
   .setWidth(430);
  DocumentApp.getUi() // Or DocumentApp or FormApp.
      .showModalDialog(html, 'Settings');
  }
  else {
    openAccount();
    if(isLoggedIn()) openSettings();
  }
}

function openAccount()
{
   var html = HtmlService.createHtmlOutputFromFile('Account');
   DocumentApp.getUi() // Or DocumentApp or FormApp.
      .showModalDialog(html, 'Account');
}

/*function openHelp()
{
   var html = HtmlService.createHtmlOutputFromFile('Help')
   .setHeight(220)
   .setWidth(410);
   DocumentApp.getUi() // Or DocumentApp or FormApp.
      .showModalDialog(html, 'Help for Math4Docs');
}*/

function onInstall(e) {
  onOpen(e);
}

function doUndo(undoText)
{
  DocumentApp.getActiveDocument().getBody().setText(undoText);
}


/**
 * Opens a sidebar in the document containing the add-on's user interface.
 * This method is only used by the regular add-on, and is never called by
 * the mobile add-on version.
 */
function showSidebar() {
  saveUserPreferences("startClicked", "true");
  if(isLoggedIn()) {
    var ui = HtmlService.createHtmlOutputFromFile('Sidebar')
      .setTitle('Check your math');
    DocumentApp.getUi().showSidebar(ui);
  }
  else {
    openAccount();
  }
}

/**
 * Gets the text the user has selected. If there is no selection,
 * this function displays an error message.
 *
 * @return {Array.<string>} The selected text.
 */
function getSelectedText() {
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var text = [];
    var elements = selection.getSelectedElements();
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].isPartial()) {
        var element = elements[i].getElement().asText();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();

        text.push(element.getText().substring(startIndex, endIndex + 1));
      } else {
        var element = elements[i].getElement();
        // Only translate elements that can be edited as text; skip images and
        // other non-text elements.
        if (element.editAsText) {
          var elementText = element.asText().getText();
          // This check is necessary to exclude images, which return a blank
          // text element.
          if (elementText != '') {
            text.push(elementText);
          }
        }
      }
    }
    if (text.length == 0) {
      throw 'Please select some text.';
    }
    return text;
  } else {
    throw 'Please select some text.';
  }
}

function getSelectionStartIndex() {
  var startIndex = 0;
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var text = [];
    var elements = selection.getSelectedElements();
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].isPartial()) {
        startIndex = elements[i].getStartOffset();
      }
    }
  }
  return startIndex;
}

function getSurroundingText() {
   var cursor = DocumentApp.getActiveDocument().getCursor();
   var surroundingText = cursor.getSurroundingText().getText();
   var surroundingTextOffset = cursor.getSurroundingTextOffset();
   surroundingText = surroundingText.substring(0,surroundingTextOffset);
   //cursor.getSurroundingText().editAsText().setUnderline(0, surroundingTextOffset-2, true);
  
   return surroundingText;
  
}
	
function getCurrentLine()
{
  if(DocumentApp.getActiveDocument().getSelection())
  {
    //DocumentApp.getUi().alert("true");
    return true;
  }
  else
  {
   var currentLine = DocumentApp.getActiveDocument().getCursor().getElement().asText().getText();
   var surroundingText = getSurroundingText();
   return [currentLine , surroundingText];
  }
}

function underlineText() {
  
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  var rangeBuilder = doc.newRange();
  
  var cursor = DocumentApp.getActiveDocument().getCursor();
  var surroundingText = cursor.getSurroundingText().getText();
  var surroundingTextOffset = cursor.getSurroundingTextOffset();
  surroundingText = surroundingText.substring(0,surroundingTextOffset);
 
  // add an extra space
  rangeBuilder.addElement(DocumentApp.getActiveDocument().getCursor().getElement(),0,DocumentApp.getActiveDocument().getCursor().getOffset()-1);
  //rangeBuilder.addElement(DocumentApp.getActiveDocument().);
  DocumentApp.getActiveDocument().setSelection(rangeBuilder.build());
  
  
   /*var doc = DocumentApp.getActiveDocument();
   var body = doc.getBody();
  
   /*body.appendParagraph('New paragraph.');*/
  
   /*var cursor = DocumentApp.getActiveDocument().getCursor();
   var surroundingText = cursor.getSurroundingText().getText();
   var surroundingTextOffset = cursor.getSurroundingTextOffset();
   surroundingText = surroundingText.substring(0,surroundingTextOffset);
   //cursor.getSurroundingText().editAsText()
  var rangeBuilder = doc.newRange();
  rangeBuilder.addElement(doc.getCursor().getSurroundingText().findText("test").getElement());
  DocumentApp.getActiveDocument().setSelection(rangeBuilder.build());*/
  
   //setUnderline(0, surroundingTextOffset-2, true);
   //return true;
   return true;
}


function highlightText(start,end) {
  
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  var rangeBuilder = doc.newRange();
  
  var cursor = DocumentApp.getActiveDocument().getCursor();
  var surroundingText = cursor.getSurroundingText().getText();
  var surroundingTextOffset = cursor.getSurroundingTextOffset();
  surroundingText = surroundingText.substring(0,surroundingTextOffset);
 
  // add an extra space
  rangeBuilder.addElement(DocumentApp.getActiveDocument().getCursor().getElement(),start,DocumentApp.getActiveDocument().getCursor().getOffset()+end-2);
  //rangeBuilder.addElement(DocumentApp.getActiveDocument().);
  DocumentApp.getActiveDocument().setSelection(rangeBuilder.build());
   return true;
}

/**
 * Gets the stored user preferences for the origin and destination languages,
 * if they exist.
 * This method is only used by the regular add-on, and is never called by
 * the mobile add-on version.
 *
 * @return {Object} The user's origin and destination language preferences, if
 *     they exist.
 */
function getPreferences() {
  var userProperties = PropertiesService.getUserProperties();
  var languagePrefs = {
    originLang: userProperties.getProperty('originLang'),
    destLang: userProperties.getProperty('destLang')
  };
  return languagePrefs;
}

/**
 * Gets the user-selected text and translates it from the origin language to the
 * destination language. The languages are notated by their two-letter short
 * form. For example, English is 'en', and Spanish is 'es'. The origin language
 * may be specified as an empty string to indicate that Google Translate should
 * auto-detect the language.
 *
 * @param {string} origin The two-letter short form for the origin language.
 * @param {string} dest The two-letter short form for the destination language.
 * @param {boolean} savePrefs Whether to save the origin and destination
 *     language preferences.
 * @return {Object} Object containing the original text and the result of the
 *     translation.
 */
function getTextAndTranslation(origin, dest, savePrefs) {
  var result = {};
  var text = getSelectedText();
  result['text'] = text.join('\n');

  if (savePrefs == true) {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty('originLang', origin);
    userProperties.setProperty('destLang', dest);
  }

  result['translation'] = translateText(result['text'], origin, dest);
  
  return result;
}


//Saves the user properties to PropertiesService
function saveUserPreferences(key, value) {
  var userProperties = PropertiesService.getUserProperties();
  userProperties.setProperty(key, value);
}

//Gets the user properties
function getUserPreferences(key,isArray) {
  var userProperties = PropertiesService.getUserProperties();
  var dataArray = [];
  if(isArray)
  {
    for(var i =0 ;i<key.length ; i++)
    {
     //DocumentApp.getUi().alert(userProperties.getProperty(key[i]));
      dataArray.push(userProperties.getProperty(key[i]));
    }
    
    return dataArray;
  }
 
 
  var data = userProperties.getProperty(key);
  return data;
}

// Delete the user properties
function deleteUserPreferences(key) {
  var userProperties = PropertiesService.getUserProperties();
  userProperties.deleteProperty(key);
}

// Delete all the user properties
function deleteAllUserPreferences(key) {
  var userProperties = PropertiesService.getUserProperties();
  userProperties.deleteAllProperties();
}

function getText()
{
  var text = getSelectedText();
  return text;
}

function getMarketRates()
{
  var res = UrlFetchApp.fetch("https://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json");
  var content = res.getContentText();
  return content;
  //DocumentApp.getUi().alert(content);
}

function getWholeBodyText()
{
  var startClicked = getUserPreferences("startClicked", false);
  var allText = DocumentApp.getActiveDocument().getBody().editAsText().getText();
  
  var currentLine;
  try{
    currentLine = DocumentApp.getActiveDocument().getCursor().getElement().asText().getText();
  }catch(exc){
    currentLine = "";
  }
  
  var surroundingText;
  try{
    surroundingText = getSurroundingText();
  }catch(err){
    surroundingText = "";
  }
  
  if(DocumentApp.getActiveDocument().getSelection())
  {
    return [true, allText, surroundingText, currentLine];
  }
  else
  {
    if(startClicked == "true")
    {
      //DocumentApp.getUi().alert("startClicked");
      saveUserPreferences("startClicked", "false");
      return [true, allText, surroundingText, currentLine];
    }
    else
      return [false, allText, surroundingText, currentLine];
  }
}

function selectText(start,end)
{
  //DocumentApp.getUi().alert(DocumentApp.getActiveDocument().getBody().editAsText().getText());
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  var rangeBuilder = doc.newRange();
  rangeBuilder.addElement(DocumentApp.getActiveDocument().getBody().editAsText(), start, end);
  DocumentApp.getActiveDocument().setSelection(rangeBuilder.build());
  return true;
}

function selectTextAtCursor(start,end)
{
  var doc = DocumentApp.getActiveDocument();
  var rangeBuilder = doc.newRange();
  rangeBuilder.addElement(doc.getCursor().getElement().asText().editAsText(), start, end-1);
  DocumentApp.getActiveDocument().setSelection(rangeBuilder.build());
  return true;
}

/**
 * Replaces the text of the current selection with the provided text, or
 * inserts text at the current cursor location. (There will always be either
 * a selection or a cursor.) If multiple elements are selected, only inserts the
 * translated text in the first element that can contain text and removes the
 * other elements.
 *
 * @param {string} newText The text with which to replace the current selection.
 */
function insertText(newText) {
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var replaced = false;
    var elements = selection.getSelectedElements();
    if (elements.length == 1 &&
        elements[0].getElement().getType() ==
        DocumentApp.ElementType.INLINE_IMAGE) {
        throw "Can't insert text into an image.";
    }
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].isPartial()) {
        var element = elements[i].getElement().asText();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();

        var remainingText = element.getText().substring(endIndex + 1);
        
        var tempNewText = newText.split("=");
        var expr;
        if(tempNewText.length == 2)
          expr = tempNewText[0];
        else if(tempNewText.length == 3)
          expr = tempNewText[0] + "=" + tempNewText[1];
        
        var selectedText = element.getText().substring(startIndex, endIndex + 1);
        if(selectedText.indexOf(":=") == -1) selectedText = selectedText.split("=")[0];
        if(selectedText.trim().charAt(selectedText.trim().length - 1) == "=")
        {
          if(selectedText.trim().slice(0, -1).trim() !== expr.trim()) 
            return false;
        }
        else if(selectedText.trim() !== expr.trim()) 
               return false;
        
        element.deleteText(startIndex, endIndex);
        if (!replaced) {
          element.insertText(startIndex, newText);
          replaced = true;
        } else {
          // This block handles a selection that ends with a partial element. We
          // want to copy this partial text to the previous element so we don't
          // have a line-break before the last partial.
          var parent = element.getParent();
          parent.getPreviousSibling().asText().appendText(remainingText);
          // We cannot remove the last paragraph of a doc. If this is the case,
          // just remove the text within the last paragraph instead.
          if (parent.getNextSibling()) {
            parent.removeFromParent();
          } else {
            element.removeFromParent();
          }
        }
      } else {
        var element = elements[i].getElement();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();
        
        var tempNewText = newText.split("=");
        var expr;
        if(tempNewText.length == 2)
          expr = tempNewText[0];
        else if(tempNewText.length == 3)
          expr = tempNewText[0] + "=" + tempNewText[1];
        
        var selectedText = element.getText().substring(startIndex, endIndex + 1);
        if(selectedText.trim().charAt(selectedText.trim().length - 1) == "=")
        {
          if(selectedText.trim().slice(0, -1).trim() !== expr.trim()) 
            return false;
        }
        else if(selectedText.trim() !== expr.trim()) 
               return false;
        
        if (!replaced && element.editAsText) {
          // Only translate elements that can be edited as text, removing other
          // elements.
          element.clear();
          element.asText().setText(newText);
          replaced = true;
        } else {
          // We cannot remove the last paragraph of a doc. If this is the case,
          // just clear the element.
          if (element.getNextSibling()) {
            element.removeFromParent();
          } else {
            element.clear();
          }
        }
      }
    }
  } else {
    var cursor = DocumentApp.getActiveDocument().getCursor();
    var surroundingText = cursor.getSurroundingText().getText();
    var surroundingTextOffset = cursor.getSurroundingTextOffset();

    // If the cursor follows or preceds a non-space character, insert a space
    // between the character and the translation. Otherwise, just insert the
    // translation.
    if (surroundingTextOffset > 0) {
      if (surroundingText.charAt(surroundingTextOffset - 1) != ' ') {
        newText = ' ' + newText;
      }
    }
    if (surroundingTextOffset < surroundingText.length) {
      if (surroundingText.charAt(surroundingTextOffset) != ' ') {
        newText += ' ';
      }
    }
    cursor.insertText(newText);
  }
}

//for inserting data by clicking sidebar output
function insertTextforSidebarOutputClick(newText) {
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var replaced = false;
    var elements = selection.getSelectedElements();
    if (elements.length == 1 &&
        elements[0].getElement().getType() ==
        DocumentApp.ElementType.INLINE_IMAGE) {
      throw "Can't insert text into an image.";
    }
    
    //if more than 1 line wrongly selected
    if(elements.length > 1) return false;
    
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].isPartial()) {
        var element = elements[i].getElement().asText();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();

        var remainingText = element.getText().substring(endIndex + 1);
        
        var tempNewText = newText.split("=");
        var expr;
        if(tempNewText.length == 2)
          expr = tempNewText[0];
        else if(tempNewText.length == 3)
          expr = tempNewText[0] + "=" + tempNewText[1];
        
        var selectedText = element.getText().substring(startIndex, endIndex + 1);
        if(selectedText.trim().charAt(selectedText.trim().length - 1) == "=")
        {
          if(selectedText.trim().slice(0, -1).trim() !== expr.trim()) 
            return false;
        }
        else if(selectedText.split("=")[0].trim() !== expr.trim()) 
               return false;
        
        element.deleteText(startIndex, endIndex);
        if (!replaced) {
          element.insertText(startIndex, newText);
          replaced = true;
        } else {
          // This block handles a selection that ends with a partial element. We
          // want to copy this partial text to the previous element so we don't
          // have a line-break before the last partial.
          var parent = element.getParent();
          parent.getPreviousSibling().asText().appendText(remainingText);
          // We cannot remove the last paragraph of a doc. If this is the case,
          // just remove the text within the last paragraph instead.
          if (parent.getNextSibling()) {
            parent.removeFromParent();
          } 
          else {
            element.removeFromParent();
          }
        }
      } else {
        var element = elements[i].getElement();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();
        
        var tempNewText = newText.split("=");
        var expr;
        if(tempNewText.length == 2)
          expr = tempNewText[0];
        else if(tempNewText.length == 3)
          expr = tempNewText[0] + "=" + tempNewText[1];
        
        var selectedText = element.getText().substring(startIndex, endIndex + 1);
        if(selectedText.trim().charAt(selectedText.trim().length - 1) == "=")
        {
          if(selectedText.trim().slice(0, -1).trim() !== expr.trim()) 
            return false;
        }
        else if(selectedText.split("=")[0].trim() !== expr.trim()) 
               return false;
        
        if (!replaced && element.editAsText) {
          // Only translate elements that can be edited as text, removing other
          // elements.
          element.clear();
          element.asText().setText(newText);
          replaced = true;
        } else {
          // We cannot remove the last paragraph of a doc. If this is the case,
          // just clear the element.
          if (element.getNextSibling()) {
            element.removeFromParent();
          } else {
            element.clear();
          }
        }
      }
    }
    return true;
  } else {
    return false;
  }
}

function deleteText(start,end,result)
{
   DocumentApp.getActiveDocument().getCursor().getSurroundingText().deleteText(start, end-1);
   insertText(result);
}


function isSelection()
{
  return true;
}

/**
 * Given text, translate it from the origin language to the destination
 * language. The languages are notated by their two-letter short form. For
 * example, English is 'en', and Spanish is 'es'. The origin language may be
 * specified as an empty string to indicate that Google Translate should
 * auto-detect the language.
 *
 * @param {string} text text to translate.
 * @param {string} origin The two-letter short form for the origin language.
 * @param {string} dest The two-letter short form for the destination language.
 * @return {string} The result of the translation, or the original text if
 *     origin and dest languages are the same.
 */
function translateText(text, origin, dest) {
  if (origin === dest) {
    return text;
  }
  return LanguageApp.translate(text, origin, dest);
}

function insertTextAtPos(result,pos){
    //var cursor = DocumentApp.getActiveDocument().getCursor().getElement().asBody().editAsText();
    //text.insertText(10, 'What test?, lorem ipsum!'); 
    //DocumentApp.getUi().alert(result + " || " + pos);
    var surroundingText = DocumentApp.getActiveDocument().getCursor().getSurroundingText().getText().trim(" ");
    //var surroundingTextOffset = cursor.getSurroundingTextOffset();
    
    if(surroundingText[surroundingText.length -1] === "=")
    {
      var cursor = DocumentApp.getActiveDocument().getCursor().getSurroundingText().editAsText().insertText(pos, result +" ");
    }
    else
    {
      var cursor = DocumentApp.getActiveDocument().getCursor().getSurroundingText().editAsText().insertText(pos, " = " + result +" ");
    }
   //var surroundingTextOffset = cursor.getOffset();
  
   //var body = DocumentApp.getActiveDocument().getBody();
   //var text = body.editAsText();
   //text.insertText(surroundingTextOffset, surroundingTextOffset); 
}

function checkCompleted()
{
  var ui = DocumentApp.getUi(); // Same variations.
  var result = ui.alert(
     'Evaluation completed ! Click restart evaluation to evaluate from begining',
      ui.ButtonSet.OK);
}

 
function getSelectedTextLength() 
{
  var text = getSelectedText();
  return text.toString().length;
}

function logRedirectUri() {
  var service = getGitHubService();
  Logger.log(service.getRedirectUri());
}

/**
 * Gets the current cursor information for the document.
 * @return {Object} The infomration.
 */
/*function getDocumentInfo() {
  var document = DocumentApp.getActiveDocument();
  var cursor = document.getCursor();
  var result = {};
  if (cursor) {
    result.cursor = {
      surroundingText: cursor.getSurroundingText().getText(),
      offset: cursor.getSurroundingTextOffset()
    };
  }
  return result;
}*/